import React from 'react'

const CardItem = () => {
    return (
        <div className="flex-col  sm:flex-row flex p-5 card-item rounded-md border-2 border-gray-400">
            <div className="pb-60 sm:pb-0 w:full flex-none relative sm:w-48">
                <img className="absolute inset-0 rounded-md w-full h-full object-cover" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD-l889V8_Nv64SYZECELEBUzvWgmgxdlAow&usqp=CAU" alt=""/>
            </div>
            <div className="pl-0 flex-auto sm:pl-5 pb-3">
                <div className="pt-3 sm:pt-0 sm:flex-col md:flex-row header flex flex-wrap">
                    <div className="pt-0 mb-3 sm:mb-3 flex-auto sm:pt-3">
                        <h1 className="text-xl sm:text-2xl md:text-3xl lg:text-4xl font-semibold">
                            William Herlands
                        </h1>
                        <h2 className="text-lg sm:text-xl md:text-2xl font-semibold text-gray-500 ">
                            CO-Founder, BZR
                        </h2>
                    </div>
                    

                    <div className="verified-btn flex-1 flex items-start lg:justify-end">
                        <p className="text-sm text-blue-400 sm:text-lg font-semibold flex-none mr-2 ">Verified</p>
                        <svg className="w-5 flex-none sm:w-7 fill-current text-blue-400 mr-2 " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                        <path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clipRule="evenodd" />
                        </svg>
                        <div className="box border-l-2 pl-2 flex-none">
                            <svg className="w-5 sm:w-7" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                            </svg>
                        </div>
                    </div>
                </div>

                <div className="button mt-10">
                    <button className="text-sm w-full sm:w-auto hover:shadow-lg shadow-md py-2 px-4 border-transparent border-2 rounded-md sm:text-md  font-lighter text-gray-600 " >
                        Accepts 90% of meetings
                    </button>
                </div>

            </div>

        </div>
    )
}

export default CardItem
