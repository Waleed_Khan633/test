import React from 'react'

const AffilateList = ({list}) => {
    return (
        <div className="AffilateList mt-10">
            <h1 className="text-xl text-gray-600 font-semibold" >Affiliations</h1>
            <div className="flex flex-wrap mt-3">
                {list.map((v,i)=>{
                    return(
                        <React.Fragment key={i}>
                            <div className="text-sm flex-none text-gray-500 font-normal sm:text-lg">{v.key},&nbsp; {v.key=="CEO" ? <a className="underline text-gray-400" href="#">{v.value}</a> : v.value}</div>
                            <div className="dash flex-none border-r-2 h-4 m-auto"></div>
                        </React.Fragment>
                    )
                })}
            </div>
        </div>
    )
}

export default AffilateList
