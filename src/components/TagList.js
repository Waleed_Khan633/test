import React from 'react'

const TagList = ({list}) => {
    return (
       <div className="taglist mt-10">
           <h1 className="text-xl text-gray-600 font-semibold" >Industries</h1>
           <div className="flex flex-wrap mt-3">
                {list.map((v,i)=>{
                    return(
                        <React.Fragment key={i}>
                            <div className="text-sm tag cursor-pointer flex-none border-gray-800 rounded-md py-1 px-3  text-gray-400 border sm:text-md ">#{v}</div>
                        </React.Fragment>
                    )
                })}
            </div>
       </div>
    )
}

export default TagList
