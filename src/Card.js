import React from 'react'
import AffilateList from './components/AffilateList'
import CardItem from './components/CardItem'
import Taglist from './components/TagList'

const Card = () => {

  const TagList = [
    'CyberSecurity',
    'ECommerce',
    'MachineLearning',
    'VentureCapital',
    'ArtificialIntelligence'
  ]

  const AffiliatList = [
    {
      key:'Co-Founder',
      value: 'BZR'
    },
    {
      key:'CEO',
      value:'Willow'
    },
    {
      key:'Phd Machine Learning',
      value:'CMU'
    }
  ]


  return (
    <div className="card m-auto mt-5">
      <CardItem />
      <div className="w-4/5 m-auto">
        <AffilateList list={AffiliatList} />
        <Taglist list={TagList} />
        <button className="text-sm hover:bg-blue-300 focus:outline-none outline-none bg-blue-400 w-full rounded-md text-white font-semibold py-2 mt-10 sm:text-lg" >Request Metting - $250</button>
      </div>
    </div>
  )
}

export default Card
